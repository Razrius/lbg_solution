import Page from './Page.js';
const fs = require('fs');

class WeatherForecast extends Page {

    open(url) {
        super.open(url)
    }

    get cityName() {
        return $('//*[@id="city"]');
    }

    get forecastErrorMessage() {
        return $('#root > div > div');
    }

    getDayTag(day) {
        let counter = day + 2;
        let selector = '#root > div > div:nth-child(' + counter +') > div.summary > span:nth-child(1) > span.name';
        return $(selector)
    }

    getFirstElementHourlyForecast(day) {
        let counter = day + 2;
        let selector = '#root > div > div:nth-child(' + counter + ') > div.details > div:nth-child(1) > span:nth-child(1) > span';
        return $(selector);
    }

    getTemperatureMaximum(day) {
        let counter = day + 2;
        let selector = '#root > div > div:nth-child(' + counter + ') > div.summary > span:nth-child(3) > span.max';
        return $(selector);
    }
    
    getCityName() {
        return this.cityName.getValue();
    }

    getForecastErrorMessage() {
        return this.forecastErrorMessage.getText();
    }

    getTemperatureMaximumGivenDay(day){
        return this.getTemperatureMaximum(day).getText();
    }

    getForecastDayName(day) {
        return this.getDayTag(day).getText();
    }

    setCityName(name) {
        this.cityName.clearValue();
        this.cityName.setValue(name);
    }
}

export default new WeatherForecast();