# Acceptance Test Automation Assessment Solution!

In this repository you can find an attempted solution for the assessment that can be found here:

[https://github.com/buildit/acceptance-testing](https://github.com/buildit/acceptance-testing)

In order to get started please follow the instructions below:

Go to [https://github.com/buildit/acceptance-testing](https://github.com/buildit/acceptance-testing)

Set up the application following their instructions and make sure you run the application first.

Clone this repository. Enter the folder that's been created.

Run the following command in terminal the project's root folder:
`./node_modules/.bin/wdio run wdio.conf.js`   

Observe as the tests are executing.

## Notes for the solution

Basic setup is implemented for Webdriver.io and Cucumber.js. Two feature files are added.
The project was setup using page object model for separation of concerns and future scalability.
Due to time limitations several tests need to be still added, in addition to most of the edge case scenarios.

Future test runners can be added by changing the wdio config file found in the root folder of the project.
Future addition can be to import the mock files from the original web project and counter check results vs the mock data.
