Feature: 3 hourly forecast for a given day
   
    Obtaining 3hourly forecast for a given day

    Scenario: Initial city's forecast should be shown
        Given I'm on the weather forecast page
        Then I can see 5 days of weather forecast

    Scenario: Three hourly forecast is shown for all of days when the day is clicked
        Given I'm on the weather forecast page
        When I click on all of the days
        Then I can see the 3 hourly forecasts for each day