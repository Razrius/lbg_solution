import {Given, When, Then, And} from 'cucumber';
import WeatherForecast from '../pages/WeatherForecast';
const assert = require('assert');

const edinburghMaxTempMock = ['18°','16°','14°','14°','14°'];
const forecastDays = ['Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

Given(/^I'm on the weather forecast page$/, () => {
    WeatherForecast.open('http://localhost:3000/');
    let pageTitle = browser.getTitle();
    assert.equal(pageTitle, '5 Weather Forecast');
});

When(/^I set the city name to "([^"]*)"$/, (text) => {
    WeatherForecast.setCityName(text);
    browser.keys("Enter");
});

When(/^I click on all of the days$/, () => {
    for (let i=0;i<forecastDays.length;i++) {
        WeatherForecast.getDayTag(i).click();
    }
});

Then(/^Default city name is "([^"]*)"$/, (text) => {
    let cityName = WeatherForecast.getCityName();
    return assert.equal(cityName, text);
});

Then(/^Weather forecast for "([^"]*)" is shown$/, (text) => {
    for (let i=0;i<edinburghMaxTempMock.length;i++) {
        let temperature = WeatherForecast.getTemperatureMaximumGivenDay(i);
        return assert.equal(temperature, edinburghMaxTempMock[i]);
    }
});

Then(/^I should see the "([^"]*)" message$/, (text) => {
    let cityName = WeatherForecast.getForecastErrorMessage();
    return assert.equal(cityName, text);
});

Then(/^I can see 5 days of weather forecast$/, () => {
    for (let i=0;i<forecastDays.length;i++) {
        let dayName = WeatherForecast.getForecastDayName(i);
        return assert.equal(dayName, forecastDays[i]);
    }
});

Then(/^I can see the 3 hourly forecasts for each day$/, () => {
    for (let i=0;i<forecastDays.length;i++) {
        WeatherForecast.getDayTag(i).click();
        let firstElement = WeatherForecast.getFirstElementHourlyForecast(i);
        return assert.equal(firstElement.isDisplayed(), true);
    }
});