   Feature: Obtaining forecast for a city
   
     Obtaining forecast for a specific city based on entering a city name.

   Scenario: Default city Name should be Glasgow
      Given I'm on the weather forecast page
      Then Default city name is "Glasgow"
   
   Scenario: Edinburgh forecast is shown when default city name is changed to Edinburgh
      Given I'm on the weather forecast page
      When I set the city name to "Edinburgh"
      Then Weather forecast for "Edinburgh" is shown

   Scenario: When I enter invalid city name an error is shown
      Given I'm on the weather forecast page
      When I set the city name to "1234567"
      Then I should see the "Error retrieving the forecast" message